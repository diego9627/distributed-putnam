All mathematical ideas, with or without full proofs, are welcome.  Ideally, the idea should reference a Putnam problem (https://kskedlaya.org/putnam-archive/ is a good compendium).

- Please submit PRs modifying the main TeX source file.
- Be sure to recompile the PDF with `pdflatex`.