# distributed-putnam

This project aims to bring together math enthusiasts to solve hard problems with distributed collaboration.

More on our long-term goals can be found [here](purpose/purpose.pdf).

**How you can help.**  We're starting with [The Putnam](https://www.maa.org/math-competitions/putnam-competition).  We welcome PRs with problems and ideas.  All mathematical topics that pertain to problems on the Putnam are welcome.

If you want to help further, have any questions, or can give helpful feedback please make a Gitlab issue.